from parse_config import UserConfig
import os
import json
import sys
from json import JSONDecodeError
from typing import Optional, List, Tuple, Dict, Any, Union
from io import TextIOWrapper

from log import Log

log = Log()

"""用户处理"""


class User:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
        return cls._instance

    def __init__(self):
        self.__data__ = None
        self.__init_json_data__()
        self.__groups__: List[str] = UserConfig().get_groups()

    def __init_json_data__(self):
        try:
            if os.path.getsize("./Config/Users.json") == 0:
                self.__data__ = []
                return
            with open("./Config/Users.json", "r") as json_data:
                self.__data__ = json.load(json_data)
        except JSONDecodeError or ValueError or OSError or IOError:
            log.error("用户文件格式错误！")
            log.error("请手动修复或删除！文件路径 Config/Users.json")
            sys.exit(1)
        except FileNotFoundError:
            self.__data__ = {}
            open("./Config/Users.json", "w").close()
        except Exception as e:
            log.error(f"解析用户文件失败！原因：「{e}」")
            log.error("请手动修复或删除！文件路径 Config/Users.json")
            sys.exit(1)

    """
    格式化用户文件
    """

    def format(self):
        try:
            for i, USER in enumerate(self.__data__, start=1):
                USER['id'] = str(i)
            with open("./Config/Users.json", "w") as json_file:
                json_file: TextIOWrapper
                json.dump(
                    self.__data__,
                    json_file,
                    ensure_ascii=False,
                    indent=4)

        except Exception as e:
            log.error(f"格式化用户文件失败！原因：「{e}」")
            sys.exit(1)

    def check_group(self, name: str) -> bool:
        return name in self.__groups__

    def check(self):
        if self.__data__ is None:
            self.__init_json_data__()

        self.format()

        id_conflicts = []
        uid_conflicts = []
        group_errors = []
        id_set = set()
        uid_set = set()

        for USER in self.__data__:
            if USER['id'] in id_set:
                id_conflicts.append(USER)
            else:
                id_set.add(USER['id'])

            if not self.check_group(USER['group']):
                group_errors.append(USER)

            if USER['uid'] in uid_set:
                uid_conflicts.append(USER)
            else:
                uid_set.add(USER['uid'])

        if id_conflicts:
            log.error(f"用户 id 冲突！冲突项：{id_conflicts}")
        if group_errors:
            log.error(f"不存在的用户组：{group_errors}")
        if uid_conflicts:
            log.error(f"用户 uid 冲突！冲突项：{uid_conflicts}")

        if id_conflicts or group_errors or uid_conflicts:
            sys.exit(1)

    """
    获取信息
    输入值
    * way                 获取方式（id/name/uid/group）
    * connect             对应的信息
    * max_length          最大获取数量
    返回值
    * list[dict]          包含搜索到的所有列表，例如 ["id": "1", "name": "XiaoMing", "group":  "User", "uid": "this_is_uid"]
    * None                无对应值
    * (list[dict], int)   若获取方式为 group 或者 name，而 max_length 不为空，则返回对应的列表和剩余数量
    """

    def get(self,
            way: str,
            content: str = None,
            max_length: Optional[int] = None) -> Optional[Tuple[Union[Dict[str,
                                                                           Any],
                                                                      List[Dict[str,
                                                                                Any]]],
                                                                int]]:
        if way not in ['id', 'name', 'uid', 'group']:
            return None

        results = []

        if way == 'id':
            for USER in self.__data__:
                if USER['id'] == content:
                    return USER

        elif way == 'name':
            for USER in self.__data__:
                if USER['name'] == content:
                    results.append(USER)

        elif way == 'uid':
            for USER in self.__data__:
                if USER['uid'] == content:
                    return USER

        elif way == 'group':
            for USER in self.__data__:
                if USER['group'] == content:
                    results.append(USER)

        if results:
            if max_length is not None:
                actual_length = min(len(results), max_length)
                return results[:actual_length], 0
            else:
                return results, 0
        else:
            return [], 0
    """
    添加用户
    * 交互添加
    """

    def add(self, name: str, group: str, uid: str):
        print("Hello")


if __name__ == '__main__':
    user = User()
    os.path.abspath(__file__)
    print(user.get("group", "User", 7))
    print(user.get("name", "XiaoMing"))
    user.check()
